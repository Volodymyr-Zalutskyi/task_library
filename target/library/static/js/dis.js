/**
 * Notification bar
 */
$(document).ready(function(){
    $(".alert")
        .delay(2000)
        .hide('slide', {direction: 'up' },
            1800, function(){});
});


$('#exhibit-additional-filters').on('change', function() {
    if($(this).children(":selected").attr("id") === 'option-1') {
        $('#by-title').removeClass('hidden');
        $('#by-title').addClass('visible');

        $('#by-independence').addClass('hidden');

        $('#by-popular-books').addClass('hidden');

        $('#by-unpopular-books').addClass('hidden');
    } else if($(this).children(":selected").attr("id") === 'option-2') {
        $('#by-independence').removeClass('hidden');
        $('#by-independence').addClass('visible');

        $('#by-title').addClass('hidden');

        $('#by-popular-books').addClass('hidden');

        $('#by-unpopular-books').addClass('hidden');

    } else if($(this).children(":selected").attr("id") === 'option-3') {
        $('#by-popular-books').removeClass('hidden');
        $('#by-popular-books').addClass('visible');

        $('#by-title').addClass('hidden');

        $('#by-independence').addClass('hidden');

        $('#by-unpopular-books').addClass('hidden');
    } else if($(this).children(":selected").attr("id") === 'option-4') {
        $('#by-unpopular-books').removeClass('hidden');
        $('#by-unpopular-books').addClass('visible');

        $('#by-title').addClass('hidden');

        $('#by-independence').addClass('hidden');

        $('#by-popular-books').addClass('hidden');
    } else {
        $('#by-title').addClass('hidden');
        $('#by-independence').addClass('hidden');
        $('#by-popular-books').addClass('hidden');
        $('#by-unpopular-books').addClass('hidden');

    }
});


$('#select-book').on('change',function() {
    var bookId = this.value;
    $(this).parent().find('.book-instance-wrapper').remove();
    $.get('http://localhost:8080/task_library/books-instances/' + bookId , function (data) {
        $('.books-group').append(data);
    });
});

$(document).ready(function () {
    var bookId = $('#select-book').val();
    $.get('http://localhost:8080/task_library/books-instances/' + bookId , function (data) {
        $('.books-group').append(data);
    });
});
