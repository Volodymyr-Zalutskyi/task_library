package entities;

import java.time.LocalDate;

public class Order extends Model {

    private LocalDate dateOrder;
    private LocalDate dateReturn;
    private User user;
    private BookInstance bookInstance;

    public Order(Long id, LocalDate dateOrder, LocalDate dateReturn, User user, BookInstance bookInstance) {
        super(id);
        this.dateOrder = dateOrder;
        this.dateReturn = dateReturn;
        this.user = user;
        this.bookInstance = bookInstance;
    }

    public Order(LocalDate dateOrder, LocalDate dateReturn, User user, BookInstance bookInstance) {
        this.dateOrder = dateOrder;
        this.dateReturn = dateReturn;
        this.user = user;
        this.bookInstance = bookInstance;
    }

    public Order() {
        super();
    }

    public LocalDate getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(LocalDate dateOrder) {
        this.dateOrder = dateOrder;
    }

    public LocalDate getDateReturn() {
        return dateReturn;
    }

    public void setDateReturn(LocalDate dateReturn) {
        this.dateReturn = dateReturn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BookInstance getBookInstance() {
        return bookInstance;
    }

    public void setBookInstance(BookInstance bookInstance) {
        this.bookInstance = bookInstance;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + getId() +
                ", dateOrder=" + dateOrder +
                ", dateReturn=" + dateReturn +
                ", user=" + user +
                ", bookInstance=" + bookInstance +
                '}';
    }
}
