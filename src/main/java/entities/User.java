package entities;

import java.time.LocalDate;

public class User extends Model {

    private String userName;
    private String userSurname;
    private LocalDate birthday;
    private LocalDate registrationDate;
    private String phoneNumber;
    private String email;
    private Address userAddress;

    public User(Long id, String userName, String userSurname, LocalDate birthday, LocalDate registrationDate, String phoneNumber, String email, Address userAddress) {
        super(id);
        this.userName = userName;
        this.userSurname = userSurname;
        this.birthday = birthday;
        this.registrationDate = registrationDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.userAddress = userAddress;
    }

    public User(Long id, String userName, String userSurname, LocalDate birthday, LocalDate registrationDate, String phoneNumber, String email) {
        super(id);
        this.userName = userName;
        this.userSurname = userSurname;
        this.birthday = birthday;
        this.registrationDate = registrationDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public User(String userName, String userSurname, LocalDate birthday, LocalDate registrationDate, String phoneNumber, String email, Address userAddress) {
        this.userName = userName;
        this.userSurname = userSurname;
        this.birthday = birthday;
        this.registrationDate = registrationDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.userAddress = userAddress;
    }

    public User() {
        super();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(Address userAddress) {
        this.userAddress = userAddress;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", userName='" + userName + '\'' +
                ", userSurname='" + userSurname + '\'' +
                ", birthday=" + birthday +
                ", registrationDate=" + registrationDate +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", userAddress=" + userAddress +
                '}';
    }
}
