package entities;
import java.time.LocalDate;

public class Book extends Model {

    private int amountOfInstances;
    private String title;
    private LocalDate releaseDate;
    private Author author;

    public Book(Long id, int amountOfInstances, String title, LocalDate releaseDate, Author author) {
        super(id);
        this.amountOfInstances = amountOfInstances;
        this.title = title;
        this.releaseDate = releaseDate;
        this.author = author;
    }

    public Book(int amountOfInstances, String title, LocalDate releaseDate, Author author) {
        this.amountOfInstances = amountOfInstances;
        this.title = title;
        this.releaseDate = releaseDate;
        this.author = author;
    }

    public Book() {
        super();
    }

    public int getAmountOfInstances() {
        return amountOfInstances;
    }

    public void setAmountOfInstances(int amountOfInstances) {
        this.amountOfInstances = amountOfInstances;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + getId() +
                ", amountOfInstances=" + amountOfInstances +
                ", title='" + title + '\'' +
                ", releaseDate=" + releaseDate +
                ", author=" + author +
                '}';
    }
}
