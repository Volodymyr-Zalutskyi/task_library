package entities;

public class BookInstance extends Model {

    private Boolean isAvailable;
    private Book book;

    public BookInstance(Long id, Boolean isAvailable, Book book) {
        super(id);
        this.isAvailable = isAvailable;
        this.book = book;
    }

    public BookInstance(Boolean isAvailable, Book book) {
        this.isAvailable = isAvailable;
        this.book = book;
    }

    public BookInstance() {
        super();
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "BookInstance{" +
                "id=" + getId() +
                ", isAvailable=" + isAvailable +
                ", book=" + book +
                '}';
    }
}
