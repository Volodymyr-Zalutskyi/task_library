package entities;

public class Address extends Model{

    private String city;
    private String street;
    private Long buildingNumber;
    private Long apartment;

    public Address(Long id, String city, String street, Long buildingNumber, Long apartment) {
        super(id);
        this.city = city;
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.apartment = apartment;
    }

    public Address(String city, String street, Long buildingNumber, Long apartment) {
        this.city = city;
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.apartment = apartment;
    }

    public Address() {
        super();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Long buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getApartment() {
        return apartment;
    }

    public void setApartment(Long apartment) {
        this.apartment = apartment;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + getId() +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", buildingNumber=" + buildingNumber +
                ", apartment=" + apartment +
                '}';
    }
}
