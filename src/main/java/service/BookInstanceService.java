package service;

import dao.interfaceDao.BookInstanceInterfaceDao;
import database.DaoFactory;
import dto.request.BookInstanceRequest;
import dto.response.BookInstanceResponse;
import entities.BookInstance;

import java.util.List;
import java.util.stream.Collectors;

public class BookInstanceService {

    private BookInstanceInterfaceDao bookInstanceInterfaceDao;

    public  BookInstanceService() {
        this.bookInstanceInterfaceDao = DaoFactory.bookInstanceDao();
    }

    public void create(BookInstanceRequest bookInstanceRequest) {
        bookInstanceInterfaceDao.add(convertBookInstanceDtoToBookInstance(bookInstanceRequest));
    }

    public List<BookInstanceResponse> findAll() {
        return bookInstanceInterfaceDao.getAll()
                .stream()
                .map(BookInstanceResponse::new)
                .collect(Collectors.toList());
    }

    public BookInstance findById(Long id) {
        return bookInstanceInterfaceDao.getById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not exist"));
    }

    public void update(BookInstanceRequest bookInstanceRequest) {
        bookInstanceInterfaceDao.update(convertBookInstanceDtoToBookInstance(bookInstanceRequest));
    }

    public void delete(long id) {
        bookInstanceInterfaceDao.delete(id);
    }


    private BookInstance convertBookInstanceDtoToBookInstance(BookInstanceRequest bookInstanceRequest) {
        BookInstance bookInstance = new BookInstance();
        bookInstance.setAvailable(bookInstanceRequest.getAvailable());
        bookInstance.setBook(bookInstanceRequest.getBook());

        return bookInstance;
    }
}
