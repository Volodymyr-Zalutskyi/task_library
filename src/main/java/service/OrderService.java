package service;

import dao.interfaceDao.BookInstanceInterfaceDao;
import dao.interfaceDao.OrderInterfaceDao;
import dao.interfaceDao.UserInterfaceDao;
import database.DaoFactory;
import dto.request.OrderRequest;
import dto.response.OrderResponse;
import entities.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderService {

    private OrderInterfaceDao orderInterfaceDao;
    private UserInterfaceDao userInterfaceDao;
    private BookInstanceInterfaceDao bookInstanceInterfaceDao;

    public OrderService() {
        this.orderInterfaceDao = DaoFactory.orderDao();
        this.userInterfaceDao = DaoFactory.userDao();
        this.bookInstanceInterfaceDao = DaoFactory.bookInstanceDao();
    }

    public void create(OrderRequest orderRequest) {
        orderInterfaceDao.add(convertDtoToOrder(orderRequest));
    }

    public Order findOne(Long id) {
        return orderInterfaceDao.getById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not exist"));
    }

    public List<OrderResponse> findAll() {
        return orderInterfaceDao.getAll()
                .stream()
                .map(OrderResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        orderInterfaceDao.delete(id);
    }

    public void update(OrderRequest orderRequest) {
        orderInterfaceDao.update(convertDtoToOrder(orderRequest));
    }

    private Order convertDtoToOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setDateOrder(orderRequest.getDateOrder());
        order.setDateReturn(orderRequest.getDateReturn());
        order.setUser(orderRequest.getUser());
        order.setBookInstance(orderRequest.getBookInstance());

        return order;
    }

}
