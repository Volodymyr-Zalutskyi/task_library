package service;

import dao.interfaceDao.AuthorInterfaceDao;
import database.DaoFactory;
import dto.request.AuthorRequest;
import dto.response.AuthorResponse;
import entities.Author;

import java.util.List;
import java.util.stream.Collectors;

public class AuthorService {

    private AuthorInterfaceDao authorInterfaceDao;

    public AuthorService() {
        this.authorInterfaceDao = DaoFactory.authorDao();
    }

    public void create(AuthorRequest authorRequest) {
        authorInterfaceDao.add(convertAuthorDtoToAuthor(authorRequest));
    }

    public List<AuthorResponse> findAll() {
        return authorInterfaceDao.getAll()
                .stream()
                .map(AuthorResponse::new)
                .collect(Collectors.toList());
    }

    public Author findById(long id) {
        return authorInterfaceDao.getById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not exist"));
    }

    public void update(AuthorRequest authorRequest) {
        authorInterfaceDao.update(convertAuthorDtoToAuthor(authorRequest));
    }

    public  void delete(long id) {
        authorInterfaceDao.delete(id);
    }

    private Author convertAuthorDtoToAuthor(AuthorRequest authorRequest) {
        Author author = new Author();
        author.setAuthorFirstName(authorRequest.getAuthorFirstName());
        author.setAuthorLastName(authorRequest.getAuthorLastName());

        return author;
    }

}
