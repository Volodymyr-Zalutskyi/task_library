package service;

import dao.interfaceDao.AddressInterfaceDao;
import dao.interfaceDao.BookInstanceInterfaceDao;
import dao.interfaceDao.UserInterfaceDao;
import database.DaoFactory;
import dto.request.UserRequest;
import dto.response.UserResponse;
import entities.Address;
import entities.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserInterfaceDao userDao;
    private AddressInterfaceDao addressDao;
    private BookInstanceInterfaceDao bookInstanceDao;

    public UserService() {
        this.userDao = DaoFactory.userDao();
        this.addressDao = DaoFactory.addressDao();
        this.bookInstanceDao = DaoFactory.bookInstanceDao();
    }


    public void create(UserRequest userRequest, Address address) {
        addressDao.add(address);
        userRequest.setUserAddress(address);
        userDao.add(convertUserDtoToUser(userRequest));
    }

    public List<UserResponse> findAll() {
        return userDao.getAll()
                .stream()
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    public User findOne(long id) {
        return userDao.getById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not exist"));
    }


    public void update(UserRequest userRequest) {
        userDao.update(convertUserDtoToUser(userRequest));
    }

    public void delete(Long id) {
        userDao.delete(id);
    }

    private User convertUserDtoToUser(UserRequest userRequest) {
        User user = new User();
        user.setUserName(userRequest.getUserName());
        user.setUserSurname(userRequest.getUserSurname());
        user.setBirthday(userRequest.getBirthday());
        user.setRegistrationDate(userRequest.getRegistrationDate());
        user.setEmail(userRequest.getEmail());
        user.setPhoneNumber(userRequest.getPhoneNumber());
        user.setUserAddress(userRequest.getUserAddress());

        return user;
    }


}
