package service;

import dao.interfaceDao.BookInterfaceDao;
import database.DaoFactory;
import dto.request.BookInstanceRequest;
import dto.request.BookRequest;
import dto.response.BookResponse;
import entities.Book;

import java.util.List;
import java.util.stream.Collectors;

public class BookService {

    private AuthorService authorService;
    private BookInstanceService bookInstanceService;
    private UserService userService;
    private BookInterfaceDao bookDao;


    public BookService() {
        this.authorService = new AuthorService();
        this.bookInstanceService = new BookInstanceService();
        this.userService = new UserService();
        this.bookDao = DaoFactory.bookDao();
    }

    public void create(BookRequest bookRequest) {
        bookDao.add(convertDtoToBook(bookRequest));
        for(int i = 0; i < bookRequest.getAmountOfInstances(); i++) {
            BookInstanceRequest bookInstance = new BookInstanceRequest();
            bookInstance.setAvailable(true);
            bookInstance.setBook(convertDtoToBook(bookRequest));
            bookInstanceService.create(bookInstance);
        }
    }

    public Book findOne(Long id) {
        return bookDao.getById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + id + " not exist"));
    }

    public List<BookResponse> findAll() {
        return bookDao.getAll()
                .stream()
                .map(BookResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        bookDao.delete(id);
    }

    public void update(BookRequest bookRequest) {
        bookDao.update(convertDtoToBook(bookRequest));
    }

    private Book convertDtoToBook(BookRequest bookRequest) {
        Book book = new Book();
        book.setAmountOfInstances(bookRequest.getAmountOfInstances());
        book.setTitle(bookRequest.getTitle());
        book.setReleaseDate(bookRequest.getReleaseDate());
        book.setAuthor(bookRequest.getAuthor());

        return book;
    }


}
