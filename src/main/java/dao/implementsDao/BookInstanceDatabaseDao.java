package dao.implementsDao;

import dao.interfaceDao.BookInstanceInterfaceDao;
import dao.interfaceDao.BookInterfaceDao;
import entities.BookInstance;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookInstanceDatabaseDao implements BookInstanceInterfaceDao {

    private final Connection connection;

    private BookInterfaceDao bookInterfaceDao;

    private static BookInstanceDatabaseDao bookInstanceDatabaseDao;

    private BookInstanceDatabaseDao(Connection connection, BookInterfaceDao bookInterfaceDao) {
        this.connection = connection;
        this.bookInterfaceDao = bookInterfaceDao;
    }

    public static BookInstanceInterfaceDao getInstance(Connection connection, BookInterfaceDao bookInterfaceDao) {
        if (bookInstanceDatabaseDao == null) {
            bookInstanceDatabaseDao = new BookInstanceDatabaseDao(connection, bookInterfaceDao);
        }
        return bookInstanceDatabaseDao;
    }


    @Override
    public List<BookInstance> getAll() {
        String query = "SELECT * FROM book_instance";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            return extractBookInstance(resultSet).collect(Collectors.toList());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<BookInstance> getById(Long id) {
        String query = "SELECT * FROM book_instance WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBookInstance(resultSet).findAny();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(BookInstance model) {
        String insert = "INSERT INTO book_instance (is_available, id_book) VALUE(?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
            preparedStatement.setBoolean(1, model.getAvailable());
            preparedStatement.setLong(2, model.getBook().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(BookInstance model) {
        String update = "UPDATE book_instance SET isAvailable = ?, book = ? WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setBoolean(1, model.getAvailable());
            preparedStatement.setLong(2, model.getBook().getId());
            preparedStatement.setLong(3, model.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM book_instance WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Boolean isAvailable(Long id) {
        return getById(id).orElse(null).getAvailable();
    }

    @Override
    public Long getAmountOfTimesInstanceWasTaken(Long id) {
        String query_time_taken = "SELECT count(date_order) FROM orders WHERE id_book_instance = ?";
        return getaLong(id, query_time_taken, connection);
    }

    static Long getaLong(Long id, String query_time_taken, Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_time_taken)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getLong("count(date_order)");
        } catch (SQLException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<BookInstance> findAllReturnedBookInstanceByUser(Long userId) {
        String query_returned = "SELECT id_book_instance FROM users LEFT JOIN orders ON"
                + " users.id = orders.id_users WHERE date_return is not null AND id_users = ?";
        return getBookInstances(userId, query_returned);
    }

    @Override
    public List<BookInstance> findAllBookInstanceOnReading(Long userId) {
        String query_reading = "SELECT id_book_instance FROM users LEFT JOIN orders ON"
                + " users.id = orders.id_users WHERE date_return is null AND id_users = ?";
        return getBookInstances(userId, query_reading);
    }

    @Override
    public List<BookInstance> findAllBookInstanceByBookId(Long bookId) {
        String query_all_Instance_Id = "SELECT * FROM book_instance WHERE id_book = ?";
        return getBookInstances(bookId, query_all_Instance_Id);
    }

    private List<BookInstance> getBookInstances(Long Id, String query) {
        List<BookInstance> bookInstances = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, Id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {

                    bookInstances.add(getById(resultSet.getLong("id_book_instance")).get());
                }
                return bookInstances;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<BookInstance> findAllBookInstanceByTitle(String bookTitle) {
        String query_title = "SELECT book_instance.id FROM books JOIN book_instance ON books.id = book_instance.id_book WHERE title = ?";
        List<BookInstance> bookInstanceId = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_title)) {
            preparedStatement.setString(1, bookTitle);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bookInstanceId.add(
                        getById(resultSet.getLong("id")).get());
            }
            return bookInstanceId;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<BookInstance, Long> amountBookInstanceWasOrderedByPeriod(LocalDate firstDate, LocalDate secondDate) {
        String query_ordered = "SELECT id_book_instance ,  COUNT(orders.date_order) FROM orders\n" +
                "    WHERE date_order BETWEEN ? AND ?\n" +
                "    GROUP BY id_book_instance;";
        Map<BookInstance, Long> map = new HashMap<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_ordered)) {
            preparedStatement.setDate(1, java.sql.Date.valueOf(firstDate));
            preparedStatement.setDate(2, Date.valueOf(secondDate));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                map.put(getById(resultSet.getLong("id_book_instance")).get(),
                        resultSet.getLong("COUNT(orders.date_order)"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    private Stream<BookInstance> extractBookInstance(ResultSet resultSet) throws SQLException {
        Stream.Builder<BookInstance> bookInstanceStream = Stream.builder();
        while (resultSet.next()) {
            bookInstanceStream.add(new BookInstance(resultSet.getLong("id"),
                    resultSet.getBoolean("is_available"),
                    bookInterfaceDao.getById(resultSet.getLong("id_book")).get()));
        }
        resultSet.close();
        return bookInstanceStream.build();
    }
}
