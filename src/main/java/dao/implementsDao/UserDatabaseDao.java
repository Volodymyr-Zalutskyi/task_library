package dao.implementsDao;

import dao.interfaceDao.AddressInterfaceDao;
import dao.interfaceDao.BookInstanceInterfaceDao;
import dao.interfaceDao.UserInterfaceDao;
import entities.BookInstance;
import entities.User;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.Constants.MAX_DAYS_TO_RETURN;

public class UserDatabaseDao implements UserInterfaceDao {

    private final Connection connection;

    private static UserDatabaseDao userDatabaseDao;

    private BookInstanceInterfaceDao bookInstanceInterfaceDao;

    private AddressInterfaceDao addressInterfaceDao;

    private UserDatabaseDao(Connection connection, AddressInterfaceDao addressInterfaceDao, BookInstanceInterfaceDao bookInstanceInterfaceDao) {
        this.connection = connection;
        this.addressInterfaceDao = addressInterfaceDao;
        this.bookInstanceInterfaceDao = bookInstanceInterfaceDao;
    }

    public static UserDatabaseDao getInstance(Connection connection, AddressInterfaceDao addressInterfaceDao, BookInstanceInterfaceDao bookInstanceInterfaceDao) {
        if (userDatabaseDao == null) {
            userDatabaseDao = new UserDatabaseDao(connection, addressInterfaceDao, bookInstanceInterfaceDao);
        }
        return userDatabaseDao;
    }


    @Override
    public List<User> getAll() {
        String query = "SELECT * FROM users";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            return extractUsers(resultSet).collect(Collectors.toList());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> getById(Long id) {
        String query_id = "SELECT * FROM users WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_id)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractUsers(resultSet).findAny();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(User model) {
        String insert = "INSERT INTO users (user_name, user_surname, birthday, date_registration, phone_number, email, id_address) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
            preparedStatement.setString(1, model.getUserName());
            preparedStatement.setString(2, model.getUserSurname());
            preparedStatement.setDate(3, Date.valueOf(model.getBirthday()));
            preparedStatement.setDate(4, Date.valueOf(model.getRegistrationDate()));
            preparedStatement.setString(5, model.getPhoneNumber());
            preparedStatement.setString(6, model.getEmail());
            preparedStatement.setLong(7, model.getUserAddress().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(User model) {
        String update = "UPDATE users SET(userName = ?, userSurname = ?, birthday = ?, registrationDate = ?, phoneNumber = ?, email = ?, userAddress = ?) WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setString(1, model.getUserName());
            preparedStatement.setString(2, model.getUserSurname());
            preparedStatement.setDate(3, Date.valueOf(model.getBirthday()));
            preparedStatement.setDate(4, Date.valueOf(model.getRegistrationDate()));
            preparedStatement.setString(5, model.getPhoneNumber());
            preparedStatement.setString(6, model.getEmail());
            preparedStatement.setLong(7, model.getUserAddress().getId());
            preparedStatement.setLong(8, model.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM users WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer averageTimeUsingLibrary() {
        String query_time_using_library = "SELECT AVG(DATEDIFF(CURDATE(), date_registration)) FROM users";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_time_using_library)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer averageAgeOfUsers() {
        String query_evg_age_user = "SELECT AVG(YEAR(NOW()) - YEAR(birthday)) FROM users";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_evg_age_user)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer timeUsingLibraryByUser(Long userId) {
        String query_time_using_library_user = "SELECT DATEDIFF(CURDATE(), date_registration) FROM users WHERE id=?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_time_using_library_user)) {
            preparedStatement.setLong(1, userId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Integer amountOfOrdersBySomePeriod(LocalDate fromDate, LocalDate toDate) {
        String query_order_period = "SELECT COUNT(*) FROM orders WHERE date_order between ? and ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_order_period)) {
            preparedStatement.setDate(1, Optional.ofNullable((Date.valueOf(fromDate))).orElse(null));
            preparedStatement.setDate(2, Optional.ofNullable((Date.valueOf(toDate))).orElse(null));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<BookInstance, User> getBlackList() {
        String query_black_list = "SELECT id_book_instance, users.id FROM users INNER JOIN orders ON users.id = orders.id_users"
                + "where date_return is null and DATEDIFF(CURDATE(), date_order) > ?";
        Map<BookInstance, User> blackList = new LinkedHashMap<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_black_list)) {
            preparedStatement.setInt(1, MAX_DAYS_TO_RETURN);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    blackList.put(bookInstanceInterfaceDao.getById(resultSet.getLong("id_book_instance")).get(),
                            getById(resultSet.getLong("id")).get());
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return blackList;
    }

    @Override
    public Integer averageAgeUsersByAuthor(Long authorId) {
        String query_avg_age_user_author = "SELECT AVG(DATEDIFF(CURDATE(), users.birthday)) FROM"
                + "users INNER JOIN orders ON users.id = orders.id_users"
                + "orders INNER JOIN book_instance ON orders.id_book_instance = book_instance.id"
                + "book_instance INNER JOIN books ON book_instance.id_book = books.id"
                + "books INNER JOIN authors ON books.id_author = authors.id WHERE authors id = ?))";
        return getInteger(authorId, query_avg_age_user_author);
    }

    @Override
    public Integer averageAgeUsersByBook(Long bookId) {
        String query_ave_age_user_book = "SELECT AVG(DATEDIFF(CURDATE(), users.birthday)) FROM"
                + "users inner join orders on users.id = orders.id_users "
                + "inner join book_instance bi on orders.id_book_instance = bi.id "
                + "inner join books b on bi.id_book = b.id WHERE b.id = ?";
        return getInteger(bookId, query_ave_age_user_book);
    }

    private Integer getInteger(Long bookId, String query_ave_age_user_book) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_ave_age_user_book)) {
            preparedStatement.setLong(1, bookId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.next();

                return resultSet.getInt("AVG(DATEDIFF(CURDATE(), users.birthday))");

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<User> extractUsers(ResultSet resultSet) throws SQLException {
        Stream.Builder<User> users = Stream.builder();
        while(resultSet.next()) {
            users.add(new User(resultSet.getLong("id"),
                    resultSet.getString("user_name"),
                    resultSet.getString("user_surname"),
                    resultSet.getDate("birthday").toLocalDate(),
                    resultSet.getDate("date_registration").toLocalDate(),
                    resultSet.getString("phone_number"),
                    resultSet.getString("email"),
                    addressInterfaceDao.getById(resultSet.getLong("id_address")).get()));
        }
        resultSet.close();
        return users.build();
    }
}
