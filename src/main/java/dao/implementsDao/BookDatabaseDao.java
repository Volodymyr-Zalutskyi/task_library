package dao.implementsDao;

import dao.interfaceDao.AuthorInterfaceDao;
import dao.interfaceDao.BookInterfaceDao;
import entities.Book;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static constants.Constants.MAX_TOP_BOOKS;

public class BookDatabaseDao implements BookInterfaceDao {

    private final Connection connection;

    private static BookDatabaseDao bookDatabaseDao;

    private AuthorInterfaceDao authorInterfaceDao;

    private BookDatabaseDao(Connection connection, AuthorInterfaceDao authorinterfaceDao) {
        this.connection = connection;
        this.authorInterfaceDao = authorinterfaceDao;
    }

    public static BookDatabaseDao getInstance(Connection connection, AuthorInterfaceDao authorInterfaceDao) {
        if(bookDatabaseDao == null) {
            bookDatabaseDao = new BookDatabaseDao(connection, authorInterfaceDao);
        }
        return bookDatabaseDao;
    }

    @Override
    public List<Book> getAll() {
        String query = "SELECT * FROM books";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery()) {

            return extractBook(resultSet).collect(Collectors.toList());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Book> getById(Long id) {
        String query = "SELECT * FROM books WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                    return extractBook(resultSet).findAny();

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(Book model) {
        String insert = "INSERT INTO books (amount_of_instances, title, release_date, id_author) VALUES (?, ?, ?, ?)";
        try(PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
            preparedStatement.setLong(1, model.getAmountOfInstances());
            preparedStatement.setString(2, model.getTitle());
            preparedStatement.setDate(3, Optional.ofNullable(Date.valueOf(model.getReleaseDate()))
                    .orElseThrow(null));
            preparedStatement.setLong(4, model.getAuthor().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Book model) {
        String update = "UPDATE books SET amountOfInstances = ?, title = ?, releaseDate = ?, author = ? WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setLong(1, model.getAmountOfInstances());
            preparedStatement.setString(2, model.getTitle());
            preparedStatement.setDate(3, Optional.ofNullable(Date.valueOf(model.getReleaseDate()))
                    .orElseThrow(null));
            preparedStatement.setLong(4, model.getAuthor().getId());
            preparedStatement.setLong(5, model.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM books WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setSubAuthorForBook(Long bookId, Long authorId) {
        String insert_sub_author = "INSERT INTO book_sub_authors (bookId, authorId) VALUE(?, ?)";
        try(PreparedStatement preparedStatement = connection.prepareStatement(insert_sub_author)) {
            preparedStatement.setLong(1, bookId);
            preparedStatement.setLong(2, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> findAllBooksByAuthorId(Long authorId) {
        String query_book_author = "SELECT * FROM books WHERE id_author = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query_book_author)) {
            preparedStatement.setLong(1, authorId);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBook(resultSet).collect(Collectors.toList());

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Book findBookByTitle(String title) {
        String query_book_title = "SELECT * FROM books WHERE title = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query_book_title)) {
            preparedStatement.setString(1, title);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBook(resultSet).findAny().get();

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long getAmountOfTimesBookWasTaken(Long id) {
        String query_time_book_taken = "select count(date_order) from orders where id_book_instance = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_time_book_taken)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getLong("count(date_order)");
        } catch (SQLException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Integer getAverageTimeReadingBook(Long id) {
        String query_time_reading_book = "select AVG(DATEDIFF(date_return,date_order)) from orders " +
                "inner join book_instance bi on orders.id_book_instance = bi.id " +
                "inner join books b on bi.id_book = b.id where id_book = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_time_reading_book)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            return resultSet.getInt("AVG(DATEDIFF(date_return,date_order))");

        } catch (SQLException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Book getInfoByBookInstance(Long bookInstanceId) {
        String query_book_instance = "SELECT * FROM books b inner join book_instance b_i on b.id = b_i.id_book WHERE b_i.id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query_book_instance)) {
            preparedStatement.setLong(1, bookInstanceId);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBook(resultSet).findAny().get();

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> findAllByAuthorSurname(String authorSurname) {
        String query = "SELECT * FROM books JOIN authors ON id_author = authors.id WHERE last_name = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, authorSurname);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBook(resultSet).collect(Collectors.toList());

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> findAllBooksBySubAuthorId(Long authorId) {
        String query = "SELECT id_book FROM book_sub_authors WHERE id_author = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, authorId);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractBook(resultSet).collect(Collectors.toList());

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> findBookBetweenDate(LocalDate fromDate, LocalDate toDate) {
        String query_book_between = "SELECT * FROM books WHERE release_date BETWEEN ? AND ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query_book_between)) {
            preparedStatement.setDate(1, Optional.ofNullable((Date.valueOf(fromDate))).orElse(null));
            preparedStatement.setDate(2, Optional.ofNullable((Date.valueOf(toDate))).orElse(null));
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                return extractBook(resultSet).collect(Collectors.toList());
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<Book, Long> mostPopularBooks(LocalDate startPeriod, LocalDate endPeriod) {
        String query_book_des = "SELECT id_book,  COUNT(*) FROM orders " +
                "INNER JOIN book_instance bi ON orders.id_book_instance = bi.id " +
                "INNER JOIN books b ON bi.id_book = b.id " +
                "WHERE orders.date_order BETWEEN ? AND ?" +
                "GROUP BY b.id " +
                "ORDER BY COUNT(*) DESC LIMIT " +
                MAX_TOP_BOOKS + ";";
        return getBookDate(startPeriod, endPeriod, query_book_des);
    }

    @Override
    public Map<Book, Long> mostUnPopularBooks(LocalDate startPeriod, LocalDate endPeriod) {
        String query_book_asc = "SELECT id_book,  COUNT(*) FROM orders " +
                "INNER JOIN book_instance bi ON orders.id_book_instance = bi.id " +
                "INNER JOIN books b ON bi.id_book = b.id " +
                "WHERE orders.date_order BETWEEN ? AND ?" +
                "GROUP BY b.id " +
                "ORDER BY COUNT(*) ASC LIMIT " +
                MAX_TOP_BOOKS + ";";
        return getBookDate(startPeriod, endPeriod, query_book_asc);
    }

    private Map<Book, Long> getBookDate(LocalDate startPeriod, LocalDate endPeriod, String query_book_asc) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_book_asc)) {
            preparedStatement.setDate(1, Optional.ofNullable((Date.valueOf(startPeriod))).orElse(null));
            preparedStatement.setDate(2, Optional.ofNullable((Date.valueOf(endPeriod))).orElse(null));
            return getBookLongMap(preparedStatement);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<Book, Long> getBookLongMap(PreparedStatement preparedStatement) throws SQLException {
        try(ResultSet resultSet = preparedStatement.executeQuery()) {
            Map<Book, Long> bookCountMap = new LinkedHashMap<>();
            while (resultSet.next()) {
                bookCountMap.put(
                        getById(resultSet.getLong("id_book")).get(),
                        resultSet.getLong("COUNT(*)")
                );
            }
            return bookCountMap;
        }
    }

    private Stream<Book> extractBook(ResultSet resultSet) throws SQLException {
        Stream.Builder<Book> bookStream = Stream.builder();
        while (resultSet.next()) {
            bookStream.add(new Book(resultSet.getLong("id"),
                    resultSet.getInt("amount_of_instances"),
                    resultSet.getString("title"),
                    Optional.ofNullable(resultSet.getDate("release_date").toLocalDate()).orElse(null),
                    authorInterfaceDao.getById(resultSet.getLong("id_author")).get()));
        }
        resultSet.close();
        return bookStream.build();
    }
}
