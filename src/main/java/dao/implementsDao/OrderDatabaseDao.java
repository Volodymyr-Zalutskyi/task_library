package dao.implementsDao;

import dao.interfaceDao.BookInstanceInterfaceDao;
import dao.interfaceDao.OrderInterfaceDao;
import dao.interfaceDao.UserInterfaceDao;
import entities.Order;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderDatabaseDao implements OrderInterfaceDao {

    private final Connection connection;

    private static OrderDatabaseDao orderDatabaseDao;

    private UserInterfaceDao userInterfaceDao;

    private BookInstanceInterfaceDao bookInstanceInterfaceDao;

    private OrderDatabaseDao(Connection connection, UserInterfaceDao userInterfaceDao, BookInstanceInterfaceDao bookInstanceInterfaceDao) {
        this.connection = connection;
        this.userInterfaceDao = userInterfaceDao;
        this.bookInstanceInterfaceDao = bookInstanceInterfaceDao;
    }

    public static OrderDatabaseDao getInstance(Connection connection, UserInterfaceDao userInterfaceDao, BookInstanceInterfaceDao bookInstanceInterfaceDao) {
        if(orderDatabaseDao == null) {
            orderDatabaseDao = new OrderDatabaseDao(connection, userInterfaceDao, bookInstanceInterfaceDao);
        }
        return orderDatabaseDao;
    }


    @Override
    public List<Order> getAll() {
        String query = "SELECT * FROM orders";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery()) {

            return extractOrders(resultSet).collect(Collectors.toList());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Order> getById(Long id) {
        String query = "SELECT * FROM orders WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractOrders(resultSet).findAny();

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(Order model) {
        String insert = "INSERT INTO orders (date_order, date_return, id_users, id_book_instance) VALUE(?, ?, ?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
            preparedStatement.setDate( 1, Date.valueOf(model.getDateOrder()));
            preparedStatement.setDate(2, Date.valueOf(model.getDateReturn()));
            preparedStatement.setLong(3, model.getUser().getId());
            preparedStatement.setLong(4, model.getBookInstance().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Order model) {
        String update = "UPDATE orders SET dateOrder = ?, dateReturn = ?, user = ?, bookInstance = ? WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setDate( 1, Date.valueOf(model.getDateOrder()));
            preparedStatement.setDate(2, Date.valueOf(model.getDateReturn()));
            preparedStatement.setLong(3, model.getUser().getId());
            preparedStatement.setLong(4, model.getBookInstance().getId());
            preparedStatement.executeUpdate();
            preparedStatement.setLong(5, model.getId());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM orders WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void updateReturnDate(Long id, LocalDate dateReturn) {
        String update_return_date = "UPDATE orders SET dateReturn = ? WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(update_return_date)) {
            preparedStatement.setDate(1, Date.valueOf(dateReturn));
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Order> findAllByUserId(Long userId) {
        String query_userId = "SELECT * FROM orders WHERE userId = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query_userId)) {
            preparedStatement.setLong(1, userId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractOrders(resultSet).collect(Collectors.toList());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Order> findAllByUserPhoneNumber(String userPhoneNumber) {
        String query_user_phone = "SELECT * FROM orders INNER JOIN users ON orders.id_users = users.id WHERE userPhoneNumber = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query_user_phone)) {
            preparedStatement.setString(1, userPhoneNumber);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractOrders(resultSet).collect(Collectors.toList());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<Order> extractOrders(ResultSet resultSet) throws SQLException {
        Stream.Builder<Order> orders = Stream.builder();
        while (resultSet.next()) {
            orders.add(new Order(resultSet.getLong("id"),
                    resultSet.getDate("date_order").toLocalDate(),
                    resultSet.getDate("date_return").toLocalDate(),
                    userInterfaceDao.getById(resultSet.getLong("id_users")).get(),
                    bookInstanceInterfaceDao.getById(resultSet.getLong("id_book_instance")).get()));
        }
        resultSet.close();
        return orders.build();
    }

}
