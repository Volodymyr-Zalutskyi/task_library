package dao.implementsDao;

import dao.interfaceDao.AuthorInterfaceDao;
import entities.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AuthorDatabaseDao implements AuthorInterfaceDao {

    private Connection connection;

    private static AuthorDatabaseDao authorDatabaseDao;

    private AuthorDatabaseDao(Connection connection) {
        this.connection = connection;
    }

    public static AuthorDatabaseDao getInstance(Connection connection) {
        if(authorDatabaseDao == null) {
            authorDatabaseDao = new AuthorDatabaseDao(connection);
        }
        return  authorDatabaseDao;
    }

    @Override
    public List<Author> getAll() {
        String query = "SELECT * FROM authors";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery()) {
            return extractAuthor(resultSet).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Author> getById(Long id) {
        String query = "SELECT * FROM authors WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return extractAuthor(resultSet).findAny();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add(Author model) {
        String insert = "INSERT INTO authors (first_name, last_name) VALUES (?, ?)";
        try(PreparedStatement preparedStatement = connection.prepareStatement(insert)) {
            preparedStatement.setString(1, model.getAuthorFirstName());
            preparedStatement.setString(2, model.getAuthorLastName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Author model) {
        String update = "UPDATE authors SET authorFirstName = ?, authorFirstName = ? WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setString(1, model.getAuthorFirstName());
            preparedStatement.setString(2, model.getAuthorLastName());
            preparedStatement.setLong(3, model.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM authors WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Author> findBySurname(String surname) {
        String query = "SELECT * FROM authors WHERE first_name = ?";
        List<Author> authors = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, surname);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while(resultSet.next()) {
                    authors.add(new Author(resultSet.getString("first_name"), resultSet.getString("last_name")));
                }
                return authors;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Author> findAllSubAuthorByBookId(Long bookId) {
        String query = "SELECT id_book FROM book_sub_authors WHERE id_book = ?";
        List<Author> subAuthors = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, bookId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while(resultSet.next()) {
                    long id_author = resultSet.getLong("id_author");
                    subAuthors.add(getById(id_author).get());
                }
                return subAuthors;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<Author> extractAuthor(ResultSet resultSet) throws SQLException {
        Stream.Builder<Author> authorStream = Stream.builder();
        while(resultSet.next()) {
            Author author = new Author();
            author.setId(resultSet.getLong("id"));
            author.setAuthorFirstName(resultSet.getString("first_name"));
            author.setAuthorLastName(resultSet.getString("last_name"));

            authorStream.add(author);
        }
        resultSet.close();
        return  authorStream.build();
    }
}
