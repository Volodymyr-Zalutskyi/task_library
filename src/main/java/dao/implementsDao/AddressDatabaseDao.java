package dao.implementsDao;

import dao.interfaceDao.AddressInterfaceDao;
import entities.Address;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddressDatabaseDao implements AddressInterfaceDao {

    /**
     * The connection field used for interaction with database.
     */
    private final Connection connection;

    private static AddressDatabaseDao addressDatabaseDao;

    private AddressDatabaseDao(Connection connection) {
        this.connection = connection;
    }


    public static AddressDatabaseDao getInstance(Connection connection) {
        if (addressDatabaseDao == null) {
            addressDatabaseDao = new AddressDatabaseDao(connection);
        }
        return addressDatabaseDao;
    }


    @Override
    public List<Address> getAll() {
        String query = "SELECT * FROM addresses";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery()) {

            return extractAddress(resultSet).collect(Collectors.toList());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public Optional<Address> getById(Long id) {
        String query = "SELECT * from addresses where id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {

                return extractAddress(resultSet).findAny();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<Address> extractAddress(ResultSet resultSet) throws SQLException {
        Stream.Builder<Address> streamAddress = Stream.builder();
        while(resultSet.next()) {
            Address address = new Address();
            address.setId(resultSet.getLong("id"));
            address.setCity(resultSet.getString("city"));
            address.setStreet(resultSet.getString("street"));
            address.setBuildingNumber(resultSet.getLong("building_number"));
            address.setApartment(resultSet.getLong("apartment"));

            streamAddress.add(address);
        }
        resultSet.close();
        return streamAddress.build();
    }

    @Override
    public void add(Address model) {
        String insert = "INSERT INTO addresses (city, street, building_number, apartment) VALUES (?, ?, ?, ?)";
        try(PreparedStatement preparedStatement = connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, model.getCity());
            preparedStatement.setString(2, model.getStreet());
            preparedStatement.setLong(3, model.getBuildingNumber());
            preparedStatement.setLong(4, model.getApartment());
            preparedStatement.executeUpdate();
            try (ResultSet key = preparedStatement.getGeneratedKeys()) {
                if (key.next()) {
                    model.setId(key.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Address model) {
        String update = "UPDATE addresses SET city = ?, street = ?, buildingNumber = ?, apartment = ? where id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(update)) {
            preparedStatement.setString(1, model.getCity());
            preparedStatement.setString(2, model.getStreet());
            preparedStatement.setLong(3, model.getBuildingNumber());
            preparedStatement.setLong(4, model.getApartment());
            preparedStatement.setLong(5, model.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long id) {
        String delete = "DELETE FROM addresses WHERE id = ?";
        try(PreparedStatement preparedStatement = connection.prepareStatement(delete)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
