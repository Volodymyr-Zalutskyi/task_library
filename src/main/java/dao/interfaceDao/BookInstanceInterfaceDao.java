package dao.interfaceDao;

import entities.BookInstance;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface BookInstanceInterfaceDao extends ItemInterfaceDao<BookInstance> {

    Boolean isAvailable(Long id);

    Long getAmountOfTimesInstanceWasTaken(Long id);

    List<BookInstance> findAllReturnedBookInstanceByUser(Long userId);

    List<BookInstance> findAllBookInstanceOnReading(Long userId);

    List<BookInstance> findAllBookInstanceByTitle(String bookTitle);

    Map<BookInstance, Long> amountBookInstanceWasOrderedByPeriod(LocalDate firstDate, LocalDate secondDate);

    List<BookInstance> findAllBookInstanceByBookId(Long bookId);
}
