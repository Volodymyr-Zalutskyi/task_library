package dao.interfaceDao;

import entities.BookInstance;
import entities.User;

import java.time.LocalDate;
import java.util.Map;

public interface UserInterfaceDao extends ItemInterfaceDao<User> {

    Integer averageTimeUsingLibrary();

    Integer averageAgeOfUsers();

    Integer timeUsingLibraryByUser(Long userId);

    Integer amountOfOrdersBySomePeriod(LocalDate fromDate, LocalDate toDate);

    Map<BookInstance, User> getBlackList();

    Integer averageAgeUsersByAuthor(Long authorId);

    Integer averageAgeUsersByBook(Long bookId);
}
