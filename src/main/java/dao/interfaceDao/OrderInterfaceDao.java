package dao.interfaceDao;

import entities.Order;

import java.time.LocalDate;
import java.util.List;

public interface OrderInterfaceDao extends ItemInterfaceDao<Order> {

    void updateReturnDate(Long id, LocalDate dateReturn);

    List<Order> findAllByUserId(Long userId);

    List<Order> findAllByUserPhoneNumber(String userPhoneNumber);
}
