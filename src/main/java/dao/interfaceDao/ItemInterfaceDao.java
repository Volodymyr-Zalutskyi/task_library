package dao.interfaceDao;

import entities.Model;

import java.util.List;
import java.util.Optional;

public interface ItemInterfaceDao<T extends Model> {

    public List<T> getAll();

    public Optional<T> getById(Long id);

    public void add(T model);

    public void update(T model);

    public void delete(Long id);

}
