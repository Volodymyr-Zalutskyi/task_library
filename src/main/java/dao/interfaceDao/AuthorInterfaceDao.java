package dao.interfaceDao;

import entities.Author;

import java.util.List;

public interface AuthorInterfaceDao extends ItemInterfaceDao<Author> {

    List<Author> findBySurname(String surname);

    List<Author> findAllSubAuthorByBookId(Long bookId);
}
