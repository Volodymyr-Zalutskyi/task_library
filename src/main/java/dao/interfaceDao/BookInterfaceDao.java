package dao.interfaceDao;

import entities.Book;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface BookInterfaceDao extends ItemInterfaceDao<Book> {

    void setSubAuthorForBook(Long bookId, Long authorId);

    List<Book> findAllBooksByAuthorId(Long authorId);

    Book findBookByTitle(String title);

    Long getAmountOfTimesBookWasTaken(Long id);

    Integer getAverageTimeReadingBook(Long id);

    Book getInfoByBookInstance(Long bookInstanceId);

    List<Book> findAllByAuthorSurname(String authorSurname);

    List<Book> findAllBooksBySubAuthorId(Long authorId);

    List<Book> findBookBetweenDate(LocalDate fromDate, LocalDate toDate);

    Map<Book, Long> mostPopularBooks(LocalDate startPeriod, LocalDate endPeriod);

    Map<Book, Long> mostUnPopularBooks(LocalDate startPeriod, LocalDate endPeriod);

}
