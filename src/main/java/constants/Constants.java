package constants;

public class Constants {

    public static final int MAX_DAYS_TO_RETURN = 30;
    public static final int MAX_TOP_BOOKS = 3;
    public static final int DAYS_IN_YEAR = 365;
    public static final int DAYS_IN_MONTH = 30;
}
