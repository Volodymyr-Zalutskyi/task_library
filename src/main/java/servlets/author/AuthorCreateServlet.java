package servlets.author;

import dto.request.AuthorRequest;
import service.AuthorService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add-author")
public class AuthorCreateServlet extends HttpServlet {

    private AuthorService authorService;

    @Override
    public void init() throws ServletException {
        authorService = new AuthorService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/add-author.jsp");
        requestDispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        AuthorRequest authorRequest = new AuthorRequest();
        authorRequest.setAuthorFirstName(name);
        authorRequest.setAuthorLastName(surname);
        try {
            authorService.create(authorRequest);
            resp.sendRedirect(req.getContextPath() + "/authors");
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public void destroy() {
        authorService = null;
    }


}
