package servlets.user;

import dto.request.UserRequest;
import entities.Address;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/add-user")
public class UserCreateServlet extends HttpServlet {

    private UserService userService;

    public void init() throws ServletException {
        // Load the database to prepare for requests
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/add-user.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String birthday = req.getParameter("birthday");
        String registrationDate = req.getParameter("registrationDate");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        String city = req.getParameter("city");
        String street = req.getParameter("street");
        String buildingNumber = req.getParameter("buildingNumber");
        String apartment = req.getParameter("apartment");

        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setBuildingNumber(Long.parseLong(buildingNumber));
        address.setApartment(Long.parseLong(apartment));

        UserRequest userRequest = new UserRequest();
        userRequest.setUserName(name);
        userRequest.setUserSurname(surname);
        userRequest.setBirthday(LocalDate.parse(birthday));
        userRequest.setRegistrationDate(LocalDate.parse(registrationDate));
        userRequest.setPhoneNumber(phoneNumber);
        userRequest.setEmail(email);
        userRequest.setUserAddress(address);
        try {
            userService.create(userRequest, address);
            resp.sendRedirect(req.getContextPath() + "/users");
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public void destroy() {
        // Allow the database to be garbage collected
        userService = null;
    }
}
