package database;

import dao.implementsDao.*;
import dao.interfaceDao.*;
import entities.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoFactory {

    private static Connection connection;
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/epam_library?allowPublicKeyRetrieval=true&serverTimezone=UTC";
    private static final String USER = System.getenv("db_user");;
    private static final String PASSWORD = System.getenv("db_pass");

    /**
     * Private constructor used for Singleton implementation
     */
    private DaoFactory() {
    }

    static {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
//        finally {
//            try {
//                connection.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
    }

    public static AddressInterfaceDao addressDao() {
        AddressDatabaseDao addressDatabaseDao = AddressDatabaseDao.getInstance(connection);
        return addressDatabaseDao;
    }

    public static AuthorInterfaceDao authorDao() {
        AuthorDatabaseDao authorDatabaseDao = AuthorDatabaseDao.getInstance(connection);
        return authorDatabaseDao;
    }

    public static BookInterfaceDao bookDao() {
        BookDatabaseDao bookDatabaseDao = BookDatabaseDao.getInstance(connection, authorDao());
        return bookDatabaseDao;
    }

    public static BookInstanceInterfaceDao bookInstanceDao() {
        return BookInstanceDatabaseDao.getInstance(connection, bookDao());
    }

    public static UserInterfaceDao userDao() {
        UserDatabaseDao userDatabaseDao = UserDatabaseDao.getInstance(connection, addressDao(), bookInstanceDao());
        return  userDatabaseDao;
    }

    public static OrderInterfaceDao orderDao() {
        OrderDatabaseDao orderDatabaseDao = OrderDatabaseDao.getInstance(connection, userDao(), bookInstanceDao());
        return orderDatabaseDao;
    }
}
