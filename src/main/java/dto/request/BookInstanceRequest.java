package dto.request;

import entities.Book;
import entities.Model;

public class BookInstanceRequest {

    private Boolean isAvailable;
    private Book book;


    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
