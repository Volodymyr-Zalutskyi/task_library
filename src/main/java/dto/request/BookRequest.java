package dto.request;

import entities.Author;
import entities.Model;

import java.time.LocalDate;

public class BookRequest {

    private int amountOfInstances;
    private String title;
    private LocalDate releaseDate;
    private Author author;

    public int getAmountOfInstances() {
        return amountOfInstances;
    }

    public void setAmountOfInstances(int amountOfInstances) {
        this.amountOfInstances = amountOfInstances;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
