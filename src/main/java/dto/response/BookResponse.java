package dto.response;

import entities.Author;
import entities.Book;
import entities.Model;

import java.time.LocalDate;

public class BookResponse extends Model {

    private int amountOfInstances;
    private String title;
    private LocalDate releaseDate;
    private Author author;

    public BookResponse(Book book) {
        super(book.getId());
        this.amountOfInstances = book.getAmountOfInstances();
        this.title = book.getTitle();
        this.releaseDate = book.getReleaseDate();
        this.author = book.getAuthor();
    }

    public int getAmountOfInstances() {
        return amountOfInstances;
    }

    public void setAmountOfInstances(int amountOfInstances) {
        this.amountOfInstances = amountOfInstances;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
