package dto.response;

import entities.Book;
import entities.BookInstance;
import entities.Model;

public class BookInstanceResponse extends Model {

    private Boolean isAvailable;
    private Book book;

    public BookInstanceResponse(BookInstance bookInstance) {
        super(bookInstance.getId());
        this.isAvailable = bookInstance.getAvailable();
        this.book = bookInstance.getBook();
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

}
