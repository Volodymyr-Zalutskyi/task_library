package dto.response;

import entities.Address;
import entities.Model;

public class AddressResponse extends Model {

    private String city;
    private String street;
    private Long buildingNumber;
    private Long apartment;

    public AddressResponse(Address address) {
        super(address.getId());
        this.city = address.getCity();
        this.street = address.getStreet();
        this.buildingNumber = address.getBuildingNumber();
        this.apartment = address.getApartment();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Long buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getApartment() {
        return apartment;
    }

    public void setApartment(Long apartment) {
        this.apartment = apartment;
    }
}
