package dto.response;

import entities.BookInstance;
import entities.Model;
import entities.Order;
import entities.User;

import java.time.LocalDate;

public class OrderResponse extends Model {

    private LocalDate dateOrder;
    private LocalDate dateReturn;
    private User user;
    private BookInstance bookInstance;

    public OrderResponse(Order order) {
        super(order.getId());
        this.dateOrder = order.getDateOrder();
        this.dateReturn = order.getDateReturn();
        this.user = order.getUser();
        this.bookInstance = order.getBookInstance();
    }

    public LocalDate getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(LocalDate dateOrder) {
        this.dateOrder = dateOrder;
    }

    public LocalDate getDateReturn() {
        return dateReturn;
    }

    public void setDateReturn(LocalDate dateReturn) {
        this.dateReturn = dateReturn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BookInstance getBookInstance() {
        return bookInstance;
    }

    public void setBookInstance(BookInstance bookInstance) {
        this.bookInstance = bookInstance;
    }
}
