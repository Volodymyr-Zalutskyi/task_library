package dto.response;

import entities.Address;
import entities.Model;
import entities.User;

import java.time.LocalDate;

public class UserResponse extends Model {


    private String userName;
    private String userSurname;
    private LocalDate birthday;
    private LocalDate registrationDate;
    private String phoneNumber;
    private String email;
    private Address userAddress;

    public UserResponse(User user) {
        super(user.getId());
        this.userName = user.getUserName();
        this.userSurname = user.getUserSurname();
        this.birthday = user.getBirthday();
        this.registrationDate = user.getRegistrationDate();
        this.phoneNumber = user.getPhoneNumber();
        this.email = user.getEmail();
        this.userAddress = user.getUserAddress();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(Address userAddress) {
        this.userAddress = userAddress;
    }

}
