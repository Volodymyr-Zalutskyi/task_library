package dto.response;

import entities.Author;
import entities.Model;

public class AuthorResponse extends Model {

    private String authorFirstName;
    private String authorLastName;

    public AuthorResponse(Author author) {
        super(author.getId());
        this.authorFirstName = author.getAuthorFirstName();
        this.authorLastName = author.getAuthorLastName();
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }
}
