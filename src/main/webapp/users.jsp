
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<c:url value="/static/css/style.css"/>">

    <title>Users</title>
</head>
<jsp:include page="menu.jsp"/>
<body>
<section class="main-section">
    <div class="container">
        <div class="row">
            <div class="notification-bar">
                <c:if test="${not empty failureMessage}">
                    <div class="alert alert-danger" role="alert">
                            ${failureMessage}
                    </div>
                </c:if>
            </div>


            <div id="filter-panel" class="navbar bg-light rounded col-xl-12">
                <div class="form-inline">
                    <div class="form-group">
                        <a class="btn btn-primary mr-2" href="<c:url value="/add-user"/>">Create</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content">
            <table>
                <thead>
                <tr>
                    <th>First name</th>
                    <th>Second name</th>
                    <th>Phone number</th>
                    <th>City</th>
                    <th>Street</th>
                    <th>Building Number</th>
                    <th>Apartment</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td><c:out value="${user.userName}"/></td>
                            <td><c:out value="${user.userSurname}"/></td>
                            <td><c:out value="${user.phoneNumber}"/></td>
                            <td><c:out value="${user.userAddress.city}"/></td>
                            <td><c:out value="${user.userAddress.street}"/></td>
                            <td><c:out value="${user.userAddress.buildingNumber}"/></td>
                            <td><c:out value="${user.userAddress.apartment}"/></td>
                            <td><a href="/users/delete?id=<c:out value="${user.id}"/>">Delete</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="/users/update?id=<c:out value="${user.id}"/>">Update</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</section>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="<c:url value="/static/js/dis.js"/>"></script>
</html>
